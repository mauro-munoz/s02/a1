My nickname is Mau
My motivation in joining the bootcamp is to change my career and to have additional skills. I have been a Mechanical Engineer for 7 years, and would want to be a Software Engineer instead.
I believe that being a Software Engineer is a profession that is needed for the future of society. Also, I believe it pays well. So in short my motivation is direction in life and money. hehe

Work Experience:
Basic and Advance Training on Komatsu Machines (1 year)
Service Engineer Trainee at Semirara Mining and Manila Office (2 years)
Service Engineer at Dakar Senegal (2 years)
Business Application Planner at Tokyo Japan (2 years)
